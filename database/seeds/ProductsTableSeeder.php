<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1,100) as $index) {
            Product::create([
                'name' => 'Ime'. $index,
                'price' => rand(200,2000),
                'description' => 'This is some text for the LENOVO prenosnik Yoga 900 13ISK',
                'image' => "http://bg.company3g.com/p/65/65/baterija-za-lg-c1150-crna-1758.jpg",
                'category_id' => rand(1,3)
            ]);
        }
    }
}
