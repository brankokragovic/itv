<?php

use App\Order;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::create([
            'name' => 'branko',
            'date' => Carbon::today(),
            'address' => '5/E-3 New York, USA',
            'phone' => '123123123',
            'status' => 1
        ]);

        Order::create([
            'name' => 'branko2',
            'date' => Carbon::today(),
            'address' => '5/E-3 New York, USA',
            'phone' => '123123123',
            'status' => 1
        ]);

        Order::create([
            'name' => 'branko3',
            'date' => Carbon::today(),
            'address' => '577/EE-33 California, USA',
            'phone' => '123123123',
            'status' => 1
        ]);
    }
}
