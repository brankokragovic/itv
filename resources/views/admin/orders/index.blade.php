@extends('admin.layouts.master')

@section('page')
    Porudzbine
@endsection

@section('content')
    <div class="row">

        <div class="col-md-12">

            @include('admin.layouts.message')

            <div class="card">
                <div class="header">
                    <h4 class="title">Porudzbine</h4>
                    <p class="category">Lista porudzbina</p>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Korisnik</th>
                            <th>Sifra</th>
                            <th>Artikal</th>
                            <th>Kolicina</th>
                            <th>Status</th>
                            <th>Akcija</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach ($orders as $order)
                                <td>{{ $order->id }}</td>
                                <td>{{ $order->name }}</td>

                                <td>
                                @foreach ($order->products as $item)
                                    <table class="table">
                                        <tr>
                                            <td>{{$item->code}} </td>
                                            <td>{{ $item->name }}</td>

                                        </tr>
                                    </table>
                                    @endforeach
                                </td>

                                <td>
                                    @foreach ($order->orderItems as $item)
                                        <table class="table">
                                            <tr>
                                                <td>{{ $item->quantity }}</td>
                                            </tr>
                                        </table>
                                    @endforeach
                                </td>

                                <td>
                                    @if ($order->status)
                                        <span class="label label-success">Potvrdjena</span>
                                    @else
                                        <span class="label label-warning">Cekanje</span>
                                    @endif
                                </td>

                            <td>

                                @if ($order->status)
                                    {{ link_to_route('order.pending','Cekanje', $order->id, ['class'=>'btn btn-warning btn-sm']) }}
                                @else
                                    {{ link_to_route('order.confirm','Potvrdi', $order->id, ['class'=>'btn btn-success btn-sm']) }}
                                @endif

                                {{ link_to_route('orders.show','Detalji', $order->id, ['class'=>'btn btn-success btn-sm']) }}

                            </td>
                        </tr>
                        @endforeach



                        </tbody>
                    </table>

                </div>
            </div>
        </div>


    </div>
@endsection