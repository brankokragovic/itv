@extends('admin.layouts.master')

@section('page')
    Detalji porudzbine
@endsection


@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Detalji porudzbine</h4>
                    <p class="category">Detalji porudzbine</p>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Datum</th>
                            <th>Adresa</th>
                            <th>Telefon</th>
                            <th>Status</th>
                            <th>Akcija</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $order->id }}</td>
                                <td>{{ $order->date }}</td>
                                <td>{{ $order->address }}</td>
                                <td>{{ $order->phone }}</td>

                                <td>
                                    @if ($order->status)
                                        <span class="label label-success">Potvrdjena</span>
                                    @else
                                        <span class="label label-warning">Cekanje</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($order->status)
                                    {{ link_to_route('order.pending','Cekanje', $order->id, ['class'=>'btn btn-warning btn-sm']) }}
                                @else
                                    {{ link_to_route('order.confirm','Potvrdi', $order->id, ['class'=>'btn btn-success btn-sm']) }}
                                @endif  </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">

        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Detalji o artiklu</h4>
                    <p class="category">Detalji o artiklu</p>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <tr>
                            <th>ID porudzbine</th>
                            <th>Ime proizvoda</th>
                            <th>Cena</th>
                            <th>Kolicina</th>
                            <th>Slika</th>
                        </tr>
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>
                                @foreach ($order->products as $product)
                                    <table class="table">
                                        <tr>
                                            <td>{{ $product->name }}</td>
                                        </tr>
                                    </table>
                                @endforeach
                            </td>

                            <td>
                                @foreach ($order->orderItems as $item)
                                    <table class="table">
                                        <tr>
                                            <td>{{ $item->price }}</td>
                                        </tr>
                                    </table>
                                @endforeach
                            </td>
                            <td>
                                @foreach ($order->orderItems as $item)
                                    <table class="table">
                                        <tr>
                                            <td>{{ $item->quantity }}</td>
                                        </tr>
                                    </table>
                                @endforeach
                            </td>
                            <td>
                                @foreach ($order->products as $product)
                                    <table class="table">
                                        <tr>
                                            @if(json_decode($product->images  !== null))
                                                @foreach(json_decode($product->images) as $image => $value)
                                                    @if(is_array($value))
                                                        <td><img  src="{{$value[0]}}" style="width: 5em"></td>
                                                    @else
                                                        <td><img  src="{{$value}}" style="width: 5em"></td>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </tr>
                                    </table>
                                @endforeach
                            </td>
                        </tr>

                    </table>

                </div>
            </div>
        </div>
    </div>

    <a href="{{ url('/admin/orders') }}" class="btn btn-success">Nazad na porudzbine</a>
    
@endsection