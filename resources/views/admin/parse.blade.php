@extends('admin.layouts.master')

@section('page')
    Update database
@endsection

@section('content')

    <div class="row">
        <div class="col-lg-10 col-md-10">
            @include('admin.layouts.message')
            <div class="card">
                <div class="header">
                    <h4 class="title">Update database</h4>
                </div>

                <div class="content">
                    <form method="post" enctype="multipart/form-data" action="{{ url('/admin/update') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <table class="table">
                                <tr>
                                    <td width="40%" align="right"><label>Select File for Upload</label></td>
                                    <td width="30">
                                        <input type="file" name="xls" />
                                    </td>
                                    <td width="30%" align="left">
                                        <input type="submit" name="upload" class="btn btn-primary" value="Upload">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40%" align="right"></td>
                                    <td width="30"><span class="text-muted">.xls, .xslx</span></td>
                                    <td width="30%" align="left"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </form>


                </div>
            </div>
        </div>
    </div>


@endsection