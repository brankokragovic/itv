<div class="sidebar" data-background-color="white" data-active-color="danger">

    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="" class="simple-text">
                ITV Shop Admin
            </a>
        </div>

        <ul class="nav">
            <li>
                <a href="{{ url('/admin') }}">
                    <i class="ti-panel"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/products/create') }}">
                    <i class="ti-archive"></i>
                    <p>Dodaj proizvod</p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/products') }}">
                    <i class="ti-view-list-alt"></i>
                    <p>Pogledaj proizvode</p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/orders') }}">
                    <i class="ti-calendar"></i>
                    <p>Porudzbine</p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/parse') }}">
                    <i class="fa fa-users"></i>
                    <p>Update database</p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/actions') }}">
                    <i class="fa fa-users"></i>
                    <p>Akcije</p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/bestsellers') }}">
                    <i class="fa fa-users"></i>
                    <p>Najprodavaniji</p>
                </a>
            </li>
            <li>
                <a href="{{ url('/admin/banners') }}">
                    <i class="fa fa-users"></i>
                    <p>Baneri</p>
                </a>
            </li>
        </ul>
    </div>
</div>
