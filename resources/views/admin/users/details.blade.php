@extends('admin.layouts.master')

@section('page')
    Detalji o porudzini
@endsection

@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">{{ $orders[0]->user->name }} Detalji o porudzini</h4>
                    <p class="category">Lista registrovanih korisnika</p>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID porudzbine</th>
                            <th>Ime proizvoda</th>
                            <th>Kolicina</th>
                            <th>Ukupna cena</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>

                                <td>{{ $order->id }}</td>

                                <td>
                                    @foreach ($order->products as $item)
                                        <table class="table">
                                            <tr>
                                                <td>{{ $item->name }}</td>
                                            </tr>
                                        </table>
                                    @endforeach
                                </td>

                                <td>
                                    @foreach ($order->orderItems as $item)
                                        <table class="table">
                                            <tr>
                                                <td>{{ $item->quantity }}</td>
                                            </tr>
                                        </table>
                                    @endforeach
                                </td>

                                <td>
                                    @foreach ($order->orderItems as $item)
                                        <table class="table">
                                            <tr>
                                                <td>{{ $item->price }} din.</td>
                                            </tr>
                                        </table>
                                    @endforeach
                                </td>


                                <td>
                                    @if($order->status)
                                        <span class="label label-success">Potvrdjeno</span>
                                    @else
                                        <span class="label label-warning">Na cekanju</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>


@endsection