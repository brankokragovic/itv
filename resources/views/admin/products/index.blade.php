@extends('admin.layouts.master')

@section('page')
    Pogledaj proizvode
@endsection

@section('content')

    <div class="row">

        <div class="col-md-12">

            @include('admin.layouts.message')

            <div class="card">
                <div class="header">
                    <h4 class="title">Proizvodi</h4>
                    <p class="category">Lista proizvoda</p>
                    <div>
                        <form class="input-group" action="{{ url('admin/search') }}" method="GET">
                            <input type="text" name="keyword" class="form-control" placeholder="Pretrazi proizvod" minlength="3">
                            <div class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3 dropdown">

                        <a href="#" class="btn dropdown-toggle" data-toggle="dropdown">
                            Kategorije
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu"> @foreach($categories as $category)
                                @if($category->parent_id == 0 && !empty($category->name))
                                    <button><a class="dropdown-item" type="button"
                                               href="{{url('admin/category',$category->name)}}">{{$category->name}}</a></button>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Ime</th>
                            <th>Cena</th>
                            <th>Opis</th>
                            <th>Slika</th>
                            <th>Akcija</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($products as $product)
                            @if($product->name != "")
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->price }}</td>
                                <td>{{ $product->description }}</td>
                                @if(json_decode($product->images !== null ))
                                    @foreach(json_decode($product->images) as $image => $value)
                                        @if(is_array($value))
                                            <td><img class="img-thumbnail" src="{{$value[0]}}" alt="" style="width:50px;"></td>
                                        @else
                                            <td><img class="img-thumbnail" src="{{$value}}" alt="" style="width:50px;"></td>
                                        @endif
                                    @endforeach
                                @endif
                                <td>

                                    {{ Form::open(['route' => ['products.destroy', $product->id], 'method'=>'DELETE']) }}
                                        {{ Form::button('<span class="fa fa-trash"></span>', ['type'=>'submit','class'=>'btn btn-danger btn-sm','onclick'=>'return confirm("Are you sure you want to delete this?")'])  }}
                                        {{ link_to_route('products.edit','', $product->id, ['class' => 'btn btn-info btn-sm ti-pencil']) }}
                                        {{ link_to_route('products.show','', $product->id, ['class' => 'btn btn-primary btn-sm ti-list']) }}
                                    {{ Form::close() }}
                                    {{ Form::open(['route' => ['makeBestseller', $product->code], 'method'=>'POST']) }}
                                    {{ Form::button('<i class="fa fa-plus"></i>', ['type'=>'submit','class'=>'btn btn-primary btn-sm'])  }}
                                    {{ Form::close() }}


                                </td>
                            </tr>
                            @endif
                            @endforeach
                        </tbody>
                    </table>
                    @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
                        <div class="d-flex justify-content-center">
                            {{ $products->links() }}
                        </div>

                    @endif
                </div>
            </div>
        </div>


    </div>


@endsection