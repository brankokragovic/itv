@extends('admin.layouts.master')

@section('page')
    Pogledaj proizvode
@endsection

@section('content')

    <div class="row">

        <div class="col-md-12">

            @include('admin.layouts.message')

                <div class="content table-responsive table-full-width">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Ime</th>
                            <th>Cena</th>
                            <th>Opis</th>
                            <th>Popust</th>
                            <th>Slika</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($products as $product)
                            @if($product->name != "")
                                <tr>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->price }}</td>
                                    <td>{{ $product->description }}</td>
                                    <td>{{ $product->discount }} %</td>

                                @if(json_decode($product->images !== null ))
                                        @foreach(json_decode($product->images) as $image => $value)
                                            @if(is_array($value))
                                                <td><img class="img-thumbnail" src="{{$value[0]}}" alt="" style="width:50px;"></td>
                                            @else
                                                <td><img class="img-thumbnail" src="{{$value}}" alt="" style="width:50px;"></td>
                                            @endif
                                        @endforeach
                                    @endif
                                    <td>

                                        {{ Form::open(['route' => ['admin.action',$product->product_id], 'method'=>'DELETE']) }}
                                        {{ Form::button('<span class="fa fa-trash"></span>', ['type'=>'submit','class'=>'btn btn-danger btn-sm','onclick'=>'return confirm("Are you sure you want to delete this?")'])  }}
                                        {{ Form::close() }}

                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                    @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
                        <div class="d-flex justify-content-center">
                            {{ $products->links() }}
                        </div>

                    @endif
                </div>
            </div>
        </div>


    </div>


@endsection