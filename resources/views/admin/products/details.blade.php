@extends('admin.layouts.master')

@section('page')
    Dodaj akciju
@endsection

@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Dodaj akciju</h4>
                </div>
                <div class="content table-responsive table-full-width">
                    {!! Form::open(['url' => ['admin/action'], 'files'=>'true', 'method'=>'post']) !!}

                    <table class="table table-striped">
                        <tbody>

                        <tr>
                            <td>Ime</td>
                            <td>Opis</td>
                            <td>Cena</td>
                            <td>Popust(u procentima)</td>
                        </tr>
                        <tr>
                            <td style="display:none;">{!! Form::text('id', $product->id ) !!}</td>
                            <td>{!! Form::text('name', $product->name, ['class' => 'form-control', 'size' => '20']) !!}</td>
                            <td>{!! Form::text('description', $product->description, ['class' => 'form-control', 'size' => '20']) !!}</td>
                            <td>{!! Form::text('price', $product->price, ['class' => 'form-control', 'size' => '1']) !!}</td>
                            <td>{!! Form::text('discount', '', ['class' => 'form-control', 'size' => '1']) !!}</td>
                            <td style="display:none;">{!! Form::text('images', $product->images) !!}</td>
                        </tbody>

                    </table>
                    <div class="form-group">
                        {{ Form::submit('Dodaj akciju', ['class'=>'btn btn-primary']) }}
                    </div>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection