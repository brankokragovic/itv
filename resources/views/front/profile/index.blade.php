@extends('front.layouts.master')

@section('content')

    <h2>Profil</h2>
    <hr>

    <h3>Detalji o korisniku</h3>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th colspan="2">Detalji o korisniku <a href="" class="pull-right"></a></th>
        </tr>
        </thead>
        <tr>
            <th>Ime i prezime</th>
            <td>{{ $user->name}}</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>{{ $user->email }}</td>
        </tr>
        <tr>
            <th>Datum registracije</th>
            <td>{{ $user->created_at}}</td>
        </tr>
    </table>


    <h4 class="title">Porudzbine</h4>
    <hr>
    <div class="content table-responsive table-full-width">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Proizvod</th>
                <th>Kolicina</th>
                <th>Cena ukupno</th>
                <th>Status</th>
                <th>Akcija</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                @foreach ($user->order as $order)
                    <td>{{ $order->id }}</td>
                    <td>
                        @foreach ($order->products as $item)
                            <table class="table">
                                <tr>
                                    <td>{{ $item->name }}</td>
                                </tr>
                            </table>
                        @endforeach
                    </td>

                    <td>
                        @foreach ($order->orderItems as $item)
                            <table class="table">
                                <tr>
                                    <td>{{ $item->quantity }}</td>
                                </tr>
                            </table>
                        @endforeach
                    </td>

                    <td>
                        @foreach ($order->orderItems as $item)
                            <table class="table">
                                <tr>
                                    <td>{{ $item->price }} din.</td>
                                </tr>
                            </table>
                        @endforeach
                    </td>

                    <td>
                        @if ($order->status)
                            <span class="badge badge-success">Potvrdjena</span>
                        @else
                            <span class="badge badge-warning">Na cekanju</span>
                        @endif
                    </td>
                    <td>
                        <a href="{{ url('/user/order') . '/' . $order->id }}" class="btn btn-outline-dark btn-sm">Detalji</a>
                    </td>
            </tr>
            @endforeach


            </tbody>
        </table>

    </div>

@endsection