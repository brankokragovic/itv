@extends('front.layouts.master')

@section('content')
    <h2>Detalji porudzbine</h2>
    <hr>

    <div class="row">

        <div class="col-md-12">
            <h4 class="title"></h4>
            <div class="content table-responsive table-full-width">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th colspan="7">Detalji porudzbine</th>
                    </tr>
                    <tr>
                        <th>ID</th>
                        <th>Datum</th>
                        <th>Adresa</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->date }}</td>
                        <td>{{ $order->address }}</td>
                        <td>
                            @if ($order->status)
                                <span class="badge badge-success">Potvrdjena</span>
                            @else
                                <span class="badge badge-warning">Na cekanju</span>
                            @endif
                        </td>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6">

            <h4 class="title">Detalji o korisniku</h4>
            <hr>
            <div class="content table-responsive table-full-width">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Ime i prezime</th>
                        <td>{{ $order->user->name }}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{ $order->user->email }}</td>
                    </tr>
                    <tr>
                        <th>Datum registracije</th>
                        <td>{{ $order->user->created_at->diffForHumans() }}</td>
                    </tr>

                    </thead>
                </table>
            </div>
        </div>
        <div class="col-md-6">

            <h4 class="title">Detalji proizvoda</h4>
            <hr>
            <div class="content table-responsive table-full-width">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>ID porudzbenice</th>
                        <th>Ime proizvoda</th>
                        <th>Cena</th>
                        <th>Kolicina</th>
                        <th>Slika</th>
                    </tr>
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>
                            @foreach ($order->products as $product)
                                <table class="table">
                                    <tr>
                                        <td>{{ $product->name }}</td>
                                    </tr>
                                </table>
                            @endforeach
                        </td>

                        <td>
                            @foreach ($order->orderItems as $item)
                                <table class="table">
                                    <tr>
                                        <td>{{ $item->price }} din.</td>
                                    </tr>
                                </table>
                            @endforeach
                        </td>

                        <td>
                            @foreach ($order->orderItems as $item)
                                <table class="table">
                                    <tr>
                                        <td>{{ $item->quantity }}</td>
                                    </tr>
                                </table>
                            @endforeach
                        </td>

                        <td>
                            @foreach ($order->products as $product)
                                <table class="table">
                                    <tr>
                                        <td><img src="{{$product->image }}" alt=""
                                                 style="width: 2em"></td>
                                    </tr>
                                </table>
                            @endforeach
                        </td>
                    </tr>

                </table>

            </div>
        </div>
    </div>

@endsection