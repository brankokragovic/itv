@extends('front.layouts.master')

@section('style')
    <script src="https://js.stripe.com/v3/"></script>
@endsection

@section('content')

        <h2 class="mt-5"><i class="fa  fa-credit-card-alt"></i> Narucivanje</h2>
        <hr>

        <div class="row">

            <div class="col-md-7">

                @if (session()->has('msg'))
                    <div class="alert alert-success">
                        {{ session()->get('msg') }}
                    </div>
                @endif

                <h4>Vasi podaci</h4>

                <form method="post" id="payment-form" action="{{ route('checkout') }}">

                    @csrf

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="name">Ime i prezime</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Ime i prezime" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address">Adresa</label>
                        <input type="text" class="form-control" id="address" name="address" placeholder="Ulica i broj" required>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-5">
                            <label for="city">Grad</label>
                            <input type="text" class="form-control" id="city" name="city" placeholder="Grad" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="province">Opstina</label>
                            <input type="text" class="form-control" id="province" name="province" placeholder="Opstina" required>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="postal">Postanski broj</label>
                            <input type="text" class="form-control" id="postal" name="postal" placeholder="Postanski broj" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone">Broj telefona</label>
                        <input type="text" class="form-control" id="phone" placeholder="Broj telefona" name="phone" required>
                    </div>
                    <hr>
                    <button type="submit" class="btn btn-outline-info col-md-12">Zavrsi porucivanje</button>
                </form>

            </div>

            <div class="col-md-5">

                <h4>Vasa narudzbenica</h4>

                <table class="table your-order-table">
                    <tr>
                        <th>Slika</th>
                        <th>Detalji</th>
                        <th>Kolicina</th>
                    </tr>

                    @foreach (Cart::instance('default')->content() as $item )
                    <tr>
                        @if(json_decode($item->model->images  !== null))
                            @foreach(json_decode($item->model->images) as $image => $value)
                                @if(is_array($value))
                                    <td><img  src="{{$value[0]}}" style="width: 5em"></td>
                                @else
                                    <td><img  src="{{$value}}" style="width: 5em"></td>
                                @endif
                            @endforeach
                        @endif
                        <td>
                            <strong>{{ $item->model->name }}</strong><br>
                            {{ $item->model->description }} <br>
                            <span class="text-dark">{{ $item->total() }} din.</span>
                        </td>
                        <td>
                            <span class="badge badge-light">{{ $item->qty }}</span>
                        </td>
                    </tr>
                    @endforeach
                </table>

                <hr>
                <table class="table your-order-table table-bordered">
                    <tr>
                        <th colspan="2">Detalji cene</th>
                    </tr>
                    <tr>
                        <td>Cena</td>
                        <td>{{ Cart::subtotal() }} din.</td>
                    </tr>
                    <tr>
                        <td>Porez</td>
                        <td>{{ Cart::tax() }} din.</td>
                    </tr>
                    <tr>
                        <th>Ukupno</th>
                        <th>{{ Cart::total() }} din.</th>
                    </tr>

                </table>

            </div>
        </div>

    <div class="mt-5"><hr></div>

@endsection

@section('script')
    {{--<script>--}}
        {{--// Create a Stripe client.--}}
        {{--var stripe = Stripe('pk_test_4paokl8kcBC4qZqwl6yYFty3');--}}

        {{--// Create an instance of Elements.--}}
        {{--var elements = stripe.elements();--}}

        {{--// Custom styling can be passed to options when creating an Element.--}}
        {{--// (Note that this demo uses a wider set of styles than the guide below.)--}}
        {{--var style = {--}}
            {{--base: {--}}
                {{--color: '#32325d',--}}
                {{--lineHeight: '18px',--}}
                {{--fontFamily: '"Helvetica Neue", Helvetica, sans-serif',--}}
                {{--fontSmoothing: 'antialiased',--}}
                {{--fontSize: '16px',--}}
                {{--'::placeholder': {--}}
                    {{--color: '#aab7c4'--}}
                {{--}--}}
            {{--},--}}
            {{--invalid: {--}}
                {{--color: '#fa755a',--}}
                {{--iconColor: '#fa755a'--}}
            {{--}--}}
        {{--};--}}

        {{--// Create an instance of the card Element.--}}
        {{--var card = elements.create('card', {--}}
            {{--style: style,--}}
            {{--hidePostalCode: true--}}
        {{--});--}}

        {{--// Add an instance of the card Element into the `card-element` <div>.--}}
        {{--card.mount('#card-element');--}}

        {{--// Handle real-time validation errors from the card Element.--}}
        {{--card.addEventListener('change', function(event) {--}}
            {{--var displayError = document.getElementById('card-errors');--}}
            {{--if (event.error) {--}}
                {{--displayError.textContent = event.error.message;--}}
            {{--} else {--}}
                {{--displayError.textContent = '';--}}
            {{--}--}}
        {{--});--}}

        {{--// Handle form submission.--}}
        {{--var form = document.getElementById('payment-form');--}}
        {{--form.addEventListener('submit', function(event) {--}}
            {{--event.preventDefault();--}}

            {{--var options = {--}}
                {{--name: document.getElementById("name_on_card").value,--}}
                {{--address_line_1: document.getElementById("address").value,--}}
                {{--address_city: document.getElementById("city").value,--}}
                {{--address_state: document.getElementById("province").value,--}}
                {{--address_zip: document.getElementById("postal").value--}}
            {{--};--}}

            {{--stripe.createToken(card, options).then(function(result) {--}}
                {{--if (result.error) {--}}
                    {{--// Inform the user if there was an error.--}}
                    {{--var errorElement = document.getElementById('card-errors');--}}
                    {{--errorElement.textContent = result.error.message;--}}
                {{--} else {--}}
                    {{--// Send the token to your server.--}}
                    {{--stripeTokenHandler(result.token);--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}

        {{--function stripeTokenHandler(token) {--}}
            {{--// Insert the token ID into the form so it gets submitted to the server--}}
            {{--var form = document.getElementById('payment-form');--}}
            {{--var hiddenInput = document.createElement('input');--}}
            {{--hiddenInput.setAttribute('type', 'hidden');--}}
            {{--hiddenInput.setAttribute('name', 'stripeToken');--}}
            {{--hiddenInput.setAttribute('value', token.id);--}}
            {{--form.appendChild(hiddenInput);--}}

            {{--// Submit the form--}}
            {{--form.submit();--}}
        {{--}--}}
    {{--</script>--}}
@endsection