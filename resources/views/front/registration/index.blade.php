@extends('front.layouts.master')

@section('content')

    <div class="row">

    <div class="col-md-12" id="register">

        <div class="card col-md-8">
            <div class="card-body">
                <h2 class="card-title">Registruj se</h2>
                <hr>

                @if ( $errors->any() )

                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>

                @endif

                <form action="/user/register" method="post">

                    @csrf

                    <div class="form-group">
                        <label for="name">Ime i prezime:</label>
                        <input type="text" name="name" placeholder="Ime i prezime" id="name" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="text" name="email" placeholder="Email" id="email" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="password">Lozinka:</label>
                        <input type="password" name="password" placeholder="Lozinka" id="password" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="password_confirmation">Potvrdi lozinku:</label>
                        <input type="password" name="password_confirmation" placeholder="Lozinka" id="password_confirmation" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="address">Adresa:</label>
                        <textarea name="address" placeholder="Adresa" id="address" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-outline-info col-md-2"> Registruj se</button>
                    </div>

                </form>

            </div>
        </div>

    </div>

</div>

@endsection
