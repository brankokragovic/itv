@extends('front.layouts.master')

@section('content')

    <h2 class="mt-5"><i class="fa fa-shopping-cart"></i>  Korpa</h2>
    <hr>

    @if ( Cart::instance('default')->count() > 0 )

        <h4 class="mt-5">{{ Cart::instance('default')->count() }} proizvod(a) u korpi</h4>

        <div class="cart-items">

            <div class="row">

                <div class="col-md-12">

                    @if ( session()->has('msg') )

                        <div class="alert alert-success">{{ session()->get('msg') }}</div>

                    @endif

                    @if ( session()->has('errors') )

                        <div class="alert alert-warning">{{ session()->get('errors') }}</div>

                    @endif

                    <table class="table">

                        <tbody>

                        @foreach (Cart::instance('default')->content() as $item )

                            <tr>
                                @if(json_decode($item->model->images  !== null))
                                    @foreach(json_decode($item->model->images) as $image => $value)
                                        @if(is_array($value))
                                            <td><img  src="{{$value[0]}}" style="width: 5em"></td>
                                        @else
                                            <td><img  src="{{$value}}" style="width: 5em"></td>
                                        @endif
                                    @endforeach
                                @endif
                                <td>
                                    <strong>{{ $item->model->name }}</strong><br> {!! $item->model->description !!}
                                </td>
                                    <td>

                                        <form action="{{ route('cart.destroy', $item->rowId) }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-outline-danger">Ukloni</button>
                                        </form>

                                    </td>

                                <td>
                                    <select class="form-control quantity" style="width: 4.7em" data-id="{{ $item->rowId }}">
                                       @for ($i = 1; $i < 5 + 1; $i++)
                                        <option {{ $item->qty == $i ? 'selected' : '' }}>{{$i}}</option>
                                      @endfor

                                    </select>
                                </td>

                                <td>{{ $item->total() }} din.</td>
                            </tr>
                        @endforeach


                        </tbody>

                    </table>

                </div>
                <!-- Price Details -->
                <div class="col-md-6">
                    <div class="sub-total">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th colspan="2">Detalji cene</th>
                            </tr>
                            </thead>
                            <tr>
                                <td>Cena</td>
                                <td>{{ Cart::subtotal() }} din.</td>
                            </tr>
                            <tr>
                                <td>Porez</td>
                                <td>{{ Cart::tax() }} din.</td>
                            </tr>
                            <tr>
                                <th>Ukupno</th>
                                <th>{{ Cart::total() }} din.</th>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- Save for later  -->
                <div class="col-md-12">
                    <a href="/" class="btn btn-outline-dark">Nastavi sa kupovinom</a>
                    <a href="/checkout" class="btn btn-outline-info">Nastavi na porucivanje</a>
                    <hr>
                </div>
                @else
                    <h3>Nema proizvoda u vasoj korpi</h3>
                    <a href="/" class="btn btn-outline-dark">Nastavi sa kupovinom</a>
                    <hr>
                @endif

            </div>

        </div>

@endsection

@section('script')

    <script src="{{ asset('js/app.js') }}"></script>
    <script>

        const className = document.querySelectorAll('.quantity');

        Array.from(className).forEach(function (el) {
            el.addEventListener('change', function () {
                const id = el.getAttribute('data-id');
                axios.patch(`/cart/update/${id}`, {
                    quantity: this.value
                })
                    .then(function (response) {
//                        console.log(response);
                          location.reload();
                    })

                    .catch(function (error) {
                        console.log(error);
                        location.reload();
                    });
            });
        });


    </script>
@endsection