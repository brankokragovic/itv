<ul class="nested"> 
    @foreach($subcategories as $subcategory)
        <li>
            @if(count($subcategory -> childrenCategories))
                <span class="caret">{{$subcategory -> name}}</span>
            @else
                <span><a href="{{url('category',$subcategory->name)}}"> {{$subcategory -> name}} </a></span>
            @endif

            @if(count($subcategory -> childrenCategories))
                @include('front.subCategoryList', ['subcategories' => $subcategory -> childrenCategories])
            @endif
        </li>
    @endforeach
</ul>