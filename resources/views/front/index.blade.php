@extends('front.layouts.master')

{{--@include('front.layouts.header')--}}

@section('content')

    @if ( session()->has('msg') )
        <div class="alert alert-success">{{ session()->get('msg') }}</div>
    @endif

    <b id="mySidenav" class="sidenav">
        <div class="treeview w-20 border">
            @include('front.categoryTree')
        </div>
    </b>
    {{--<div>--}}
    {{--<span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; KATEGORIJE</span>--}}
    {{--</div>--}}


    {{--<form class="input-group" action="{{ url('search') }}" method="GET">--}}
        {{--<input type="text" name="keyword" class="form-control" placeholder="Pretrazi proizvod" minlength="3">--}}
        {{--<div class="input-group-append">--}}
            {{--<button class="btn btn-secondary" type="submit">--}}
                {{--<i class="fa fa-search"></i>--}}
            {{--</button>--}}
        {{--</div>--}}
    {{--</form>--}}

    @isset($bestseller)
        <div>
            @include('front.layouts.bestseller')
        </div>
    @endisset
    @isset($pocetna)
        <div style="text-align: center; font-size:30px;"><span>NAJNOVIJI PROIZVODI</span></div>
        <br>
    @endisset

    <div class="col-6  " style="padding-bottom: 20px; padding-top: 20px"></div>
    <div class="col-md-3 col-sm-pull-9"></div>
    <div class="row text-center" >

        @foreach ($products as $product)
            <div class="col-lg-2 col-md-6 mb-4">
                <div class="card product-container">

                    @if($product->on_action)
                        <div class="card-header" style="background-color : rgba(240, 0, 0, 0.56)">
                            <strong>Akcija</strong>
                        </div>
                    @endif
                    @if (!empty(json_decode($product->images)) )
                        @foreach(json_decode($product->images) as $image => $value)
                            @if(is_array($value))
                                <img class="card-img-top" data-toggle="modal"
                                     data-id="{{ $product->id }}"
                                     data-target="#product_view_{{ $product->id }}" src="{{$value[0]}}" alt="" style="cursor: pointer">
                            @else(!empty($value))
                                <img class="card-img-top" data-toggle="modal"
                                     data-id="{{ $product->id }}"
                                     data-target="#product_view_{{ $product->id }}" src="{{$value}}" alt="" style="cursor: pointer">
                            @endif
                        @endforeach
                    @else
                        <img class="card-img-top" data-toggle="modal"
                             data-id="{{ $product->id }}"
                             data-target="#product_view_{{ $product->id }}"
                             src="{{url('http://bg.company3g.com/klase/timthumb.php?src=images/noimgmala.jpg&h=300')}}"
                             alt="" style="cursor: pointer">
                    @endif

                    <div class="card-body">
                        <h5 class="card-title">{{ $product->name }}</h5>
                        <p class="card-text">
                            {!! $product->description !!}
                        </p>
                    </div>
                    <div class="card-footer">

                        @if($product->on_action)
                            Stara cena: {{$product->price}} din.
                            <br>
                            <strong>{{ $product->action_price}} din.</strong>
                        @else
                            <p><strong>{{ $product->price }} din.</strong></p>

                        @endif

                        <form action="{{ route('cart') }}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{ $product->id }}">
                            <input type="hidden" name="name" value="{{ $product->name }}">
                            @if($product->on_action)
                                <input type="hidden" name="price"
                                       value="{{ $product->action_price }}">
                            @else
                                <input type="hidden" name="price" value="{{ $product->price }}">
                            @endif
                            <button type="submit" class="btn btn-primary btn-outline-dark"><i
                                        class="fa fa-cart-plus "></i>
                                Dodaj u korpu
                            </button>
                            {{--<button type="button" class="btn btn-primary btn-outline-dark" data-toggle="modal"--}}
                                    {{--data-id="{{ $product->id }}"--}}
                                    {{--data-target="#product_view_{{ $product->id }}"><i class="fa fa-search"></i>Pogledaj--}}
                            {{--</button>--}}
                        </form>
                    </div>
                </div>
                <div class="modal fade product_view" id="product_view_{{ $product->id }}">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <img class="logo" src="{{ URL::to('/assets/img/logo.png') }}" alt="logo">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">×</span></button>
                            </div>
                            <div class="modal-body">
                                @include('front.product', ['$product' => $product])

                                <div class="card-body">
                                    <h5 class="card-title">{{ $product->name }}</h5>
                                    <p class="card-text">
                                        {!! $product->description !!}
                                    </p>
                                </div>
                                <div>
                                    @if($product->on_action)
                                        Stara cena: {{$product->price}} din.
                                        <br>
                                        <strong>{{ $product->action_price}} din.</strong>
                                    @else
                                        <p><strong>{{ $product->price }} din.</strong></p>

                                    @endif
                                </div>
                            </div>
                            <div class="modal-footer">
                                <form action="{{ route('cart') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $product->id }}">
                                    <input type="hidden" name="name" value="{{ $product->name }}">
                                    @if($product->on_action)
                                        <input type="hidden" name="price"
                                               value="{{  $product->action_price  }}">
                                    @else
                                        <input type="hidden" name="price" value="{{ $product->price }}">
                                    @endif
                                    <button type="submit" class="btn btn-primary btn-outline-dark"><i
                                                class="fa fa-cart-plus "></i>
                                        Dodaj u korpu
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    @if($products instanceof \Illuminate\Pagination\LengthAwarePaginator )
        <div class="d-flex justify-content-center">
            {{ $products->links() }}
        </div>
    @endif

@endsection


<script>

    function openNav() {
        document.getElementById("mySidenav").classList.add("opened-nav")
    }

    function closeNav() {
        document.getElementById("mySidenav").classList.remove("opened-nav")
    }

</script>


