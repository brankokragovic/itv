<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top hero-nav" style="background-color: black !important;">
    <div class="container">
        <a class="navbar-brand logo-container" href="{{ url('/') }}"><img class="logo" src="{{ URL::to('/assets/img/logo.png') }}" alt="logo"></a>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/cart"><i class="fa fa-shopping-cart"></i> Korpa
                        @if (Cart::instance('default')->count() > 0)
                            <strong>
                                ({{ Cart::instance('default')->count() }})
                            </strong>
                        @endif
                    </a>
                </li>
            </ul>
    </div>
</nav>



