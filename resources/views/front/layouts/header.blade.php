<div  class="navbar navbar-expand-lg navbar-dark bg-dark" style="background-color: black !important;">
    <div class="row position-menu">
        <button style="margin-right: 15px;" class="navbar-toggler" type="button" onclick="openNav()"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse hide" >
            <div>
                <div class="input-group-append" style="padding-bottom: 20px">
                    <button class="btn btn-outline" style="background-color: #FF9800"  onclick="openNav()" >&#9776; Proizvodi</button>
                </div>
            </div>
            <div class="col col-2"></div>
        </div>
        <div>
            <form class="input-group" style="margin: 0;" action="{{ url('search') }}" method="GET">
                <input type="text" name="keyword" class="form-control" placeholder="Pretrazi proizvod" minlength="3">
                <div class="input-group-append">
                    <button class="btn btn-secondary" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </form>
            <div class="col col-3"></div>
        </div>
    </div>
</div>