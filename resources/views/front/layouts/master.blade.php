<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Dobro dosli u omiljeni online shop.Pronadjite sve za vas mobilni, PC ili notebook po najpovoljnijim cenama. Posetite nas vec danas!">
    <meta name="author" content="ITV">
    <meta name="keywords" content="Dzojstici,Bluetooth zvucnici,Bluetooth slusalice,Wireless punjaci,Back up baterije,Karaoke,Slusalice,Konzole za igranje,Cooler za laptop,Akciona kamera">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="itvshop">
    <title>ITV SHOP - mesto gde se kupovina isplati</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="{{ url('assets/css/heroic-features.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        body {
            padding-top: unset;
        }
        .sidenav {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1100;
            top: 0;
            left: 0;
            background-color: #ffffff;
            overflow-x: hidden;
            transition: 0.5s;
        }

        .sidenav .akcija {
            top: 80px;
            font-size: 24px;
            text-decoration: none;
            color: #818181;
            display: block;
            transition: 0.3s;
        }

        .sidenav .closebtn {
            z-index: 2;
            position: absolute;
            top: -6px;
            right: 13px;
            font-size: 36px;
            margin-left: 50px;
            text-decoration: none;
        }


        #category-ul ul {
            list-style-type: none;
        }

        #category-ul a {
            color: inherit;
        }

        #category-ul {
            margin: 0;
            padding: 0;
        }

        .nested {
            display: none;
        }


        .caret {
            cursor: pointer;
            user-select: none;
        }
        .caret::before {
            content: '';
            border: solid black;
            border-width: 0 3px 3px 0;
            display: inline-block;
            padding: 3px;
            transform: rotate(-45deg);
            -webkit-transform: rotate(-45deg);
            margin-right: 6px;
        }

        .caret-down::before {
            transform: rotate(45deg);
            -webkit-transform: rotate(45deg);
        }

        .active {
            display: block;
        }

        .product-container {
            transition: .3s;
        }
        .product-container:hover {
            box-shadow: 0 0 23px -3px rgba(0,0,0, .35);
        }

        .opened-nav {
            width: 350px;
        }
        .logo {
            width: 70%
        }
        .logo-container {
            position: absolute;
            left: 20px;
        }
        .hero-nav {

            position: relative;
        }
        .position-menu {
            padding-left: 24px;
        }

        @media screen and (max-height: 450px) {
            .sidenav a {font-size: 18px;}
            .sidenav {padding-top: 190px;}
            .hide {display: none}
            .logo { width: 40%}

        }
        @media screen and (max-width: 480px) {
            .position-menu { padding-left: 0;}
            .my-4 {display:none;}
        }
        @media screen and (min-width: 993px) and (max-width: 1190px){

            .btn {font-size: x-small}
        }

        .page-link {
          color: black;
          background-color: #FF9800;
        }
        .page-item.active .page-link {
            z-index: 1;
            color: #fff;
            background-color: grey;

        }
        .page-link{
            border: 1px solid black;
            color: grey;
        }
        .page-item.active .page-link a{

            color: grey;
            text-decoration: none;
        }

    </style>

    @yield('style')

</head>

<body>

@include('front.layouts.nav')

@include('front.layouts.header')
@include('front.layouts.banners')
<!-- Page Content -->
<div class="container" style="max-width: 100%">


@yield('content')
<!-- Page Features -->
    <!-- /.row -->

</div>
<!-- /.container -->

@include('front.layouts.footer')

<!-- Bootstrap core JavaScript -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

@yield('script')

</body>

</html>
