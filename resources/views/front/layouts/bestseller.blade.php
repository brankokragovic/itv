<div style="text-align: center; font-size:30px; padding-top: 20px"><span>NAJPRODAVANIJI PROIZVODI</span></div>

<div class="row text-center">
            @foreach($bestseller as $product )
                <div class="col-lg-2 col-md-6 mb-4">
                    <div class="card product-container">
                        @if($product->on_action)
                            <div class="card-header" style="background-color : rgba(240, 0, 0, 0.56)">
                                <strong>Akcija</strong>
                            </div>
                        @endif
                        @if (!empty(json_decode($product->images)) )
                            @foreach(json_decode($product->images) as $image => $value)
                                @if(is_array($value))
                                    <img class="card-img-top" data-toggle="modal"
                                         data-id="{{ $product->id }}"
                                         data-target="#product_view_{{ $product->id }}" src="{{$value[0]}}" alt="" style="cursor: pointer">
                                @else(!empty($value))
                                    <img class="card-img-top" data-toggle="modal"
                                         data-id="{{ $product->id }}"
                                         data-target="#product_view_{{ $product->id }}"  src="{{$value}}" alt="" style="cursor: pointer">
                                @endif
                            @endforeach
                        @else
                            <img class="card-img-top" data-toggle="modal"
                                 data-id="{{ $product->id }}"
                                 data-target="#product_view_{{ $product->id }}" src="{{url('http://bg.company3g.com/klase/timthumb.php?src=images/noimgmala.jpg&h=300')}}" alt="" style="cursor: pointer">
                        @endif

                        <div class="card-body">
                            <h5 class="card-title">{{ $product->name }}</h5>
                            <p class="card-text">
                                {!! $product->description !!}
                            </p>
                        </div>
                        <div class="card-footer">

                            @if($product->on_action)
                                Stara cena: {{$product->price}} din.
                                <br>
                                <strong>{{ $product->action_price}} din.</strong>
                            @else
                                <p><strong>{{ $product->price }} din.</strong></p>

                            @endif

                            <form action="{{ route('cart') }}" method="post">
                                @csrf
                                <input type="hidden" name="id" value="{{ $product->id }}">
                                <input type="hidden" name="name" value="{{ $product->name }}">
                                @if($product->on_action)
                                    <input type="hidden" name="price"
                                           value="{{ $product->action_price }}">
                                @else
                                    <input type="hidden" name="price" value="{{ $product->price }}">
                                @endif
                                <button type="submit" class="btn btn-outline-dark"><i
                                            class="fa fa-cart-plus "></i>
                                    Dodaj u korpu
                                </button>
                                {{--<button type="button" class="btn btn-outline-dark" data-toggle="modal"--}}
                                        {{--data-id="{{ $product->id }}"--}}
                                        {{--data-target="#product_view_{{ $product->id }}"><i class="fa fa-search"></i>Pogledaj--}}
                                {{--</button>--}}
                            </form>
                        </div>
                    </div>
                    <div class="modal fade product_view" id="product_view_{{ $product->id }}">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <img class="logo" src="{{ URL::to('/assets/img/logo.png') }}" alt="logo">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                </div>
                                <div class="modal-body">
                                    @include('front.product', ['$product' => $product])

                                    <div class="card-body">
                                        <h5 class="card-title">{{ $product->name }}</h5>
                                        <p class="card-text">
                                            {!! $product->description !!}
                                        </p>
                                    </div>
                                    <div>
                                        @if($product->on_action)
                                            Stara cena: {{$product->price}} din.
                                            <br>
                                            <strong>{{ $product->action_price}} din.</strong>
                                        @else
                                            <p><strong>{{ $product->price }} din.</strong></p>

                                        @endif
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <form action="{{ route('cart') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $product->id }}">
                                        <input type="hidden" name="name" value="{{ $product->name }}">
                                        @if($product->on_action)
                                            <input type="hidden" name="price"
                                                   value="{{  $product->action_price  }}">
                                        @else
                                            <input type="hidden" name="price" value="{{ $product->price }}">
                                        @endif
                                        <button type="submit" class="btn btn-primary btn-outline-dark"><i
                                                    class="fa fa-cart-plus "></i>
                                            Dodaj u korpu
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endforeach
</div>