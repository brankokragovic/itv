@isset($banners)
    @if($banners->isNotEmpty())
<style>
    .banners {
        max-width: 100%;
        height: 300px;
    }
    .banners{
        max-width: 100%;
        height: 300px;

    }
    @media screen and (max-width: 450px) {
        .carousel-item{
            max-width: 100%;
            height: auto;
        }
        .banners {
            max-width: 100%;
            height: auto;
        }
    }
    .carousel-indicators {
        display: none ;
    }
    .center {
        margin: auto;
    }
</style>
<div id="carouselExampleControls" class="carousel slide banners" data-ride="carousel">
    <ol class="carousel-indicators">
        @foreach( $banners as $photo )
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
        @endforeach
    </ol>
    <div class="carousel-inner" role="listbox">
            @foreach($banners as $photo)

            <div class="carousel-item {{ $loop->first ? 'active' : '' }} banners">
                            <img class="d-block img-fluid center" src="{{ $photo->path }}" alt="">
                        </div>

            @endforeach
    </div>
    <a class="carousel-control-prev"  href="#carouselExampleControls" role="button" data-slide="prev" >
        <span class="carousel-control-prev-icon glyphicon-blackboard" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
    @endif
@endisset
