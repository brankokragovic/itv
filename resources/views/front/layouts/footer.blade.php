<style>
    .site-footer
    {
        background-color:black;
        padding:45px 0 20px;
        font-size:15px;
        line-height:24px;
        color:#737373;
    }
    .site-footer hr
    {
        border-top-color:#bbb;
        opacity:0.5
    }
    .site-footer hr.small
    {
        margin:20px 0
    }
    .site-footer h6
    {
        color:#fff;
        font-size:16px;
        text-transform:uppercase;
        margin-top:5px;
        letter-spacing:2px
    }
    .site-footer a
    {
        color:#737373;
    }
    .site-footer a:hover
    {
        color:#3366cc;
        text-decoration:none;
    }
    .footer-links
    {
        padding-left:0;
        list-style:none
    }
    .footer-links li
    {
        display:block
    }
    .footer-links a
    {
        color:#737373
    }
    .footer-links a:active,.footer-links a:focus,.footer-links a:hover
    {
        color:#3366cc;
        text-decoration:none;
    }
    .footer-links.inline li
    {
        display:inline-block
    }
    .site-footer .social-icons
    {
        text-align:right
    }
    .site-footer .social-icons a
    {
        width:40px;
        height:40px;
        line-height:40px;
        margin-left:6px;
        margin-right:0;
        border-radius:100%;
        background-color:#FF9800;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .copyright-text
    {
        margin:0
    }
    @media (max-width:991px)
    {
        .site-footer [class^=col-]
        {
            margin-bottom:30px
        }
    }
    @media (max-width:767px)
    {
        .site-footer
        {
            padding-bottom:0
        }
        .site-footer .copyright-text,.site-footer .social-icons
        {
            text-align:center
        }
    }
    .social-icons
    {
        padding-left:0;
        margin-bottom:0;
        list-style:none;
    }
    .social-icons li
    {
        display:inline-block;
        margin-bottom:4px;

    }
    .social-icons li.title
    {
        margin-right:15px;
        text-transform:uppercase;
        color:#96a2b2;
        font-weight:700;
        font-size:13px;

    }
    .social-icons a{
        background-color:#eceeef;
        color:#818a91;
        font-size:25px;
        display:inline-block;
        line-height:44px;
        width:44px;
        height:44px;
        text-align:center;
        margin-right:8px;
        border-radius:100%;
        -webkit-transition:all .2s linear;
        -o-transition:all .2s linear;
        transition:all .2s linear
    }
    .social-icons a:active,.social-icons a:focus,.social-icons a:hover
    {
        color:#fff;
        background-color:#29aafe
    }
    .social-icons.size-sm a
    {
        line-height:34px;
        height:34px;
        width:34px;
        font-size:14px
    }



    .social-icons a.facebook:hover
    {
        background-color:#3b5998
    }
    .social-icons a.instagram:hover
    {
        background-color:#ff6666;
    }


    @media (max-width:767px)
    {
        .social-icons li.title
        {
            display:block;
            margin-right:0;
            font-weight:600
        }
    }
</style>
<footer class="site-footer">

    <div class="container">
        <div class="row">

            <div class="col-sm-12 col-md-6">
                <h6>O nama</h6>
                <p class="text-justify">ITV <i>zeli da vasa kupovina bude jednostavna. </i>Na našem sajtu vas očekuje širok asortiman IT opreme, uređaja za video nadzor i još mnogo toga što će vam ulepšati i olakšati svakodnevnicu.

                    ITV ponuda je ponuda koja će ispuniti očekivanja svakog gejmera kao i poslovnih i privatnih korisnika.

                    ITV je uvek tu da vam obezbedi vrhunski izbor proizvoda izuzetnog kvaliteta.</p>
            </div>
            <div class="col-sm-12 col-md-6">
                <h6>Politika</h6>
                <p class="text-justify">Cene na sajtu su iskazane u dinarima sa uračunatim porezom,
                    a plaćanje se vrši isključivo u dinarima.
                    Nastojimo da budemo što precizniji u opisu proizvoda,
                    prikazu slika i samih cena, ali ne možemo garantovati da su sve informacije kompletne i bez grešaka.
                    Svi artikli prikazani na sajtu su deo naše ponude i ne podrazumeva da su dostupni u svakom trenutku.
                    Raspoloživost robe i cene možete proveriti pozivanjem broja 061/15-90-394.</p>
            </div>

        </div>
        <hr>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <p class="copyright-text">Copyright &copy; 2020 All Rights Reserved by
                    <a href="http://itvshop.rs">ITV</a>.
                </p>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <ul class="social-icons">
                    <li><a class="facebook" target="_blank" rel="noopener" href="https://www.facebook.com/ITVShopRS"><i class="fa fa-facebook"></i></a></li>
                    <li><a class="instagram" target="_blank" rel="noopener" href="https://www.instagram.com/itvshop/"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="desc">
    </div>
</footer>

