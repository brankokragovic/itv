<style>
    .carousel-control-next,
    .carousel-control-prev {
        filter: invert(100%);
    }
    .carousel-indicators li {
        filter: invert(100%);
    }
    .card {
        min-height: 300px;
    }
</style>
<div id="carouselExampleControls{{$product->id}}" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        @foreach( json_decode($product->images) as $photo )
            @if(is_array($photo))
               @foreach($photo as $image)
            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>
             @endforeach
                    @endif
        @endforeach
    </ol>
    <div class="carousel-inner" role="listbox">

        @if(json_decode($product->images  !== null))
            @foreach(json_decode($product->images) as $image => $value)
                @if(is_array($value))

                            @foreach($value as $slide)
                                <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                                    <img class="d-block img-fluid" src="{{ $slide }}" alt="">
                                </div>
                            @endforeach

                @else
                    <img class="card-img-top" src="{{$value}}" alt="">
                @endif
            @endforeach
        @endif
    </div>
    <a class="carousel-control-prev"  href="#carouselExampleControls{{$product->id}}" role="button" data-slide="prev" >
        <span class="carousel-control-prev-icon glyphicon-blackboard" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls{{$product->id}}" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

