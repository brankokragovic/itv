<ul id="category-ul" class="list-group">
    <li class="list-group-item">
        <span>ITV Shop</span>
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <br>
    </li>

    <li class="list-group-item">
        <a class="akcija" style="font-size:16px;cursor:pointer" href="{{url('onaction')}}">PROIZVODI NA AKCIJI</a>
    </li>
    @foreach($categories as $category)
        @if($category -> parent_id == 0 && !empty($category->slug))
            <li class="list-group-item">
            <span class={{ count($category -> childrenCategories) > 0 ? "caret" : "" }}>{{$category -> slug}}</span>

                @if(count($category -> childrenCategories))

                    @include('front.subCategoryList', ['subcategories' => $category -> childrenCategories])
                @endif
            </li>
        @endif
    @endforeach
</ul>



<script>
    function togglerHandler() {
        let elements = document.getElementsByClassName("caret");
        
        for (let i = 0; i < elements.length; i++) {
            elements[i].addEventListener("click", function() {
                this.parentElement.querySelector(".nested").classList.toggle("active");
                this.classList.toggle("caret-down");
            });
        }
    }

    togglerHandler();
</script>