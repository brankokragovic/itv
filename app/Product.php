<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    protected $fillable = [
                'code',
                'category',
                'category1',
                'category2',
                'available',
                'name',
                'price',
                'description' ,
                'images'
    ];
}
