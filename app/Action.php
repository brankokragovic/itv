<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $fillable = [
        'product_id',
        'name',
        'description',
        'price',
        'discount',
        'images',
    ];
}
