<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bestseller extends Model
{
    protected $table = 'bestseller';


    protected $fillable = [
        'code'
    ];
}
