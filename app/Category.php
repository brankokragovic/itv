<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'parent_id'
    ];


    public function category()
    {
        return $this->hasMany(Category::class,'parent_id','id');
    }

    public function childrenCategories()
    {
        return $this->hasMany(Category::class,'parent_id','id')->with('category');
    }

}
