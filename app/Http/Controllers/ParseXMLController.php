<?php

namespace App\Http\Controllers;

use App\Category;
use App\Exports\ProductExport;
use App\Product;
use function Couchbase\defaultDecoder;
use Cyberduck\LaravelExcel\ImporterFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;

class ParseXMLController extends Controller
{
    public function parse()
    {


        return view('admin.parse');
    }

    public function parseXML(Request $request)
    {
        ini_set('max_execution_time', '300');
        ini_set('memory_limit', '-1');
        $ch2 = curl_init();
        curl_setopt($ch2, CURLOPT_URL, 'http://bg.company3g.com/xml/3gNemanjaDjurdjevic.zip');
        curl_setopt($ch2, CURLOPT_TIMEOUT, 540);
        curl_setopt($ch2, CURLOPT_HEADER, 0);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch2, CURLOPT_POST, 1);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch2, CURLOPT_SSLVERSION, 3);
        curl_setopt($ch2, CURLOPT_FOLLOWLOCATION, true);

        $httpHeader2 = array(
            "Content-Type: text/xml; charset=UTF-8",
            "Content-Encoding: UTF-8"
        );

        $zip = curl_exec($ch2);
        $this->errno = curl_getinfo($ch2, CURLINFO_HTTP_CODE);

        curl_close($ch2);
        file_put_contents('./temp.zip', $zip);
        $ZipArchive = new \ZipArchive();
        if ($ZipArchive->open('./temp.zip') === true) {
            $ZipArchive->extractTo('public/');
            $ZipArchive->close();
        } else {
            return false;
        }


        $xml = new \SimpleXMLElement(public_path('public/3gNemanjaDjurdjevic.xml'), null, true);

        $new1 = [];
        $parsed = $this->xmlToArray($xml);
        foreach ($parsed as $item) {
            foreach ($item as $a) {

                $new1[] = $a;
            }
        }
        $new2 = [];
        foreach ($new1 as $check) {
            foreach ($check as $value) {
                $new2[] = $value;

            }
        }
        $new = array();

        $arr_file = explode('.', $_FILES['xls']['name']);
        $extension = end($arr_file);

        if ('xls' == $extension) {
            $reader = new Xls();
        } else {
            $reader = new Xlsx();
        }

        $spreadsheet = $reader->load($_FILES['xls']['tmp_name']);
        $sheetData = $spreadsheet->getActiveSheet()->toArray();
        if (!empty($sheetData)) {
            for ($i = 1; $i < count($sheetData); $i++) {

                $new[$i]['Sifra'] = $sheetData[$i][0];
                $new[$i]['MP-cena'] = $sheetData[$i][2];
                $new[$i]['Grupa1'] = $sheetData[$i][3];
                $new[$i]['Grupa2'] = $sheetData[$i][4];

            }
        }
        $combined = array();
        foreach ($new as $arr) {
            $comb = array();
            foreach ($new2 as $arr2) {
                if ($arr2['sifra'] == $arr['Sifra']) {
                    $comb['sifra'] = $arr['Sifra'];
                    $comb['cena'] = $arr['MP-cena'];
                    $comb['naziv'] = $arr2['naziv'];
                    $comb['kategorija'] = str_replace('/', ' ', $arr['Grupa1']);
                    $comb['kategorija1'] = str_replace('/', ' ', $arr['Grupa2']);
                    $comb['dostupan'] = $arr2['dostupan'];
                    $comb['slike'] = json_encode($arr2['slike']);
                    $comb['opis'] = !empty($arr2['opis']) ? $arr2['opis'] : '';
                    break;
                }
            }


            $combined[] = $comb;
        }

            foreach ($combined as $combine) {
                if (!empty($combine)) {

                    $category = '';
                    $category1 = '';
                    $category2 = '';


                    if (!empty($combine['kategorija'])) {

                        $category = str_replace('/', ' ', $combine['kategorija']);
                    }

                    if (!empty($combine['kategorija1'])) {


                        $category1 = str_replace('/', ' ', $combine['kategorija1']);
                    }

                    if (!empty($combine['kategorija2'])) {

                        $category2 = str_replace('/', ' ', $combine['kategorija2']);
                    }

                    Product::updateOrCreate([
                        'code' => $combine['sifra']
                    ], [
                        'category' => $category,
                        'category1' => $category1,
                        'category2' => $category2,
                        'available' => $combine['dostupan'],
                        'name' => $combine['naziv'],
                        'price' => $combine['cena'],
                        'description' => $combine['opis'],
                        'images' => ($combine['slike'])
                    ]);
                }

            }


                return redirect()->back();

    }


    private function xmlToArray($xml, $options = array())
    {
        $defaults = array(
            'namespaceRecursive' => false,
            //setting to true will get xml doc namespaces recursively
            'removeNamespace' => false,
            //set to true if you want to remove the namespace from resulting keys (recommend setting namespaceSeparator = '' when this is set to true)
            'namespaceSeparator' => ':',
            //you may want this to be something other than a colon
            'attributePrefix' => '@',
            //to distinguish between attributes and nodes with the same name
            'alwaysArray' => array(),
            //array of xml tag names which should always become arrays
            'autoArray' => true,
            //only create arrays for tags which appear more than once
            'textContent' => '$',
            //key used for the text content of elements
            'autoText' => true,
            //skip textContent key if node has no attributes or child nodes
            'keySearch' => false,
            //optional search and replace on tag and attribute names
            'keyReplace' => false
            //replace values for above search values (as passed to str_replace())
        );
        $options = array_merge($defaults, $options);
        $namespaces = $xml->getDocNamespaces($options['namespaceRecursive']);
        $namespaces[''] = null; //add base (empty) namespace

        //get attributes from all namespaces
        $attributesArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            if ($options['removeNamespace']) {
                $prefix = '';
            }
            foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
                //replace characters in attribute name
                if ($options['keySearch']) {
                    $attributeName =
                        str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
                }
                $attributeKey = $options['attributePrefix']
                    . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                    . $attributeName;
                $attributesArray[$attributeKey] = (string)$attribute;
            }
        }

        //get child nodes from all namespaces
        $tagsArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            if ($options['removeNamespace']) {
                $prefix = '';
            }

            foreach ($xml->children($namespace) as $childXml) {
                //recurse into child nodes
                $childArray = $this->xmlToArray($childXml, $options);
                $childTagName = key($childArray);
                $childProperties = current($childArray);

                //replace characters in tag name
                if ($options['keySearch']) {
                    $childTagName =
                        str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
                }

                //add namespace prefix, if any
                if ($prefix) {
                    $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;
                }

                if (!isset($tagsArray[$childTagName])) {
                    //only entry with this key
                    //test if tags of this type should always be arrays, no matter the element count
                    $tagsArray[$childTagName] =
                        in_array($childTagName, $options['alwaysArray'], true) || !$options['autoArray']
                            ? array($childProperties) : $childProperties;
                } elseif (
                    is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
                    === range(0, count($tagsArray[$childTagName]) - 1)
                ) {
                    //key already exists and is integer indexed array
                    $tagsArray[$childTagName][] = $childProperties;
                } else {
                    //key exists so convert to integer indexed array with previous value in position 0
                    $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
                }
            }
        }

        //get text content of node
        $textContentArray = array();
        $plainText = trim((string)$xml);
        if ($plainText !== '') {
            $textContentArray[$options['textContent']] = $plainText;
        }

        //stick it all together
        $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
            ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

        //return node as array
        return array(
            $xml->getName() => $propertiesArray
        );
    }

}
