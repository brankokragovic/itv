<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;

class BannersController extends Controller
{

    public function index(){

        return view('admin.banners.index');

    }
    public function addBanner(Request $request){

            $this->validate($request, [
                'input_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            if ($request->hasFile('input_img')) {
                $image = $request->file('input_img');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/banners');
                $image->move($destinationPath, $name);

                $banner = new Banner();
                $banner->path = '/banners/'.$name;
                $banner->save();

                return back()->with('success','Image Upload successfully');
            }
    }


    public function deleteBanner(){




    }


    public function getAllBanners(){

    }
}
