<?php

namespace App\Http\Controllers;

use App\Bestseller;
use App\Product;
use Illuminate\Http\Request;

class BestsellerController extends Controller
{
   public function makeBestsellers($code){

       if (Bestseller::where('code', $code)->exists()) {
           return redirect()->back()->with('msg','Proizvod je vec u najprodavanijim');
       }

       Bestseller::create([
           'code' => $code
       ]);

       return redirect()->back()->with('msg','Proizvod je dodat u najprodavanije');
   }



   public function removeFromBestsellers($code){

       Bestseller::where('code',$code)->delete();

       return redirect()->back()->with('msg','Proizvod je obrisan iz u najprodavanijih');

   }


   public function getBestsellers(){

       $best = Bestseller::all();

       $products = array();

       foreach ($best as $code){

           $products[] = Product::where('code',$code->code)->first();

       }

       return view('admin.products.bestseller',compact('products'));

   }
}
