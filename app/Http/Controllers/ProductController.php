<?php

namespace App\Http\Controllers;

use App\Action;
use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index() {

        $products = Product::paginate(18);
        $category = Category::with('childrenCategories')->get();
        $categories = [];
        foreach ($category as $cat){

            $categories[]= $cat;
        }


        return view('admin.products.index', compact('products','categories'));
    }

    public function create() {
        $product = new Product();
        return view('admin.products.create', compact('product'));
    }

    public function store(Request $request) {

        // Validate the form
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'description' => 'required',
            'image' => 'image|required'
        ]);

        // Upload the image
        if ($request->hasFile('image')) {
            $image = $request->image;
            $image->move('uploads', $image->getClientOriginalName());
        }

        // Save the data into database
        Product::create([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
            'image' => $request->image->getClientOriginalName()

        ]);

        // Sessions Message
        $request->session()->flash('msg','Your product has been added');

        // Redirect

        return redirect('admin/products/create');

    }

    public function edit($id) {
        $product = Product::find($id);
        return view('admin.products.edit', compact('product'));
    }

    public function update(Request $request, $id) {

        // Find the product
        $product = Product::find($id);
        // Validate The form
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'description' => 'required',
        ]);

//        // Check if there is any image
//        if ($request->hasFile('image')) {
//            // Check if the old image exists inside folder
//            if (file_exists(public_path('uploads/') . $product->image)) {
//                unlink(public_path('uploads/') . $product->image);
//            }
//
//            // Upload the new image
//            $image = $request->image;
//            $image->move('uploads', $image->getClientOriginalName());
//
//            $product->image = $request->image->getClientOriginalName();
//        }

        // Updating the product
        $product->update([
           'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
        ]);

        // Store a message in session
        $request->session()->flash('msg', 'Product has been updated');

        // Redirect
        return redirect('admin/products');

    }

    public function show($id) {
        $product = Product::find($id);
        return view('admin.products.details', compact('product'));
    }

    public function destroy($id) {
        // Delete the product
        Product::destroy($id);

        // Store a message
        session()->flash('msg','Product has been deleted');

        // Redirect back
        return redirect('admin/products');


    }

    public function search(){
        $keyword = $_GET['keyword'];
        $products = Product::where('name','like', '%' . $keyword . '%')
            ->get();
        $category = Category::with('childrenCategories')->get();
        $categories = [];
        foreach ($category as $cat){

            $categories[]= $cat;
        }

        return view('admin.products.index', compact('products','categories'));

    }

    public function category($id){
        $products = Product::where('category',$id)
            ->orWhere('category1',$id)
            ->orWhere('category2',$id)
            ->where('available','Da')
            ->paginate(18);

        $category = Category::with('childrenCategories')->get();
        $categories = [];
        foreach ($category as $cat){

            $categories[]= $cat;
        }

        return view('admin.products.index', compact('products','categories'));

    }

    public function actionCreate(Request $request){
        $new_price = $request->price - ($request->price * ($request->discount / 100));
        Action::updateOrCreate([
            'product_id' => $request->id],[
            'name'=> $request->name,
            'price' =>  $new_price,
            'images' => $request->images,
            'description' => $request->description,

            'discount' => $request->discount
        ]);

        Product::where('id',$request->id)->update(['action_price'=>$new_price,'on_action'=>true]);

        session()->flash('msg','Akcija dodata');

        return redirect()->route('admin');
    }


    public function deleteAction($product_id){
        Action::where('product_id',$product_id)->delete();

        Product::where('id',$product_id)->update(['action_price'=>'0.00','on_action'=>false]);

        return redirect()->back();
    }

    public function actions(){

      $products = Action::all();

      return view('admin.products.actions',compact('products'));

    }

}
