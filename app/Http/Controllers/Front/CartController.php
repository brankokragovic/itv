<?php

namespace App\Http\Controllers\Front;

use App\Banner;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    public function index() {

        $banners = Banner::all();
        return view('front.cart.index' ,compact($banners));
    }

    public function store(Request $request) {

        $dubl = Cart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->id === $request->id;
        });

        if ($dubl->isNotEmpty()) {
            return redirect()->back()->with('msg','Proizvod je vec u korpi');
        }

        Cart::add($request->id, $request->name, 1, $request->price)->associate('App\Product');

        return redirect()->back()->with('msg','Proizvod je dodat u korpu');

    }

    public function destroy($id) {

        Cart::remove($id);

        return redirect()->back()->with('msg','Proizvod je uklonjen');

    }

    public function saveLater($id) {

        $item = Cart::get($id);

        Cart::remove($id);

        $dubl = Cart::instance('saveForLater')->search(function ($cartItem, $rowId) use ($id) {
            return $cartItem->id === $id;
        });

        if ($dubl->isNotEmpty()) {
            return redirect()->back()->with('msg','Proizvod je sacuvan za kasnije');
        }

        Cart::instance('saveForLater')->add($item->id, $item->name, 1, $item->price)->associate('App\Product');

        return redirect()->back()->with('msg','Item has been saved for later');

    }

    public function update(Request $request, $id){

        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric|between: 1,5'
        ]);

        if ($validator->fails()) {
            session()->flash('errors','Kolicina mora biti izmedju 1 i 5');
            return response()->json(['success' => false]);
        }

        Cart::update($id, $request->quantity);

        session()->flash('msg','Kolicina je updejtovana');

        return response()->json(['success' => true]);

    }


}
