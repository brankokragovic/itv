<?php

namespace App\Http\Controllers\Front;

use App\Banner;
use App\Bestseller;
use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index() {

        $products = Product::where('category','!=', '')->where('available','Da')->latest()
            ->limit(18)->get();

        $category = Category::with('childrenCategories')->get();

        $categories = [];
        foreach ($category as $cat){

            $categories[]= $cat;
        }

        $pocetna = '';

        $best = Bestseller::all();

        $bestseller = array();
        foreach ($best as $value){
            $bestseller[] = Product::where('code',$value->code)->first();
        }
        $banners = Banner::all();
        return view('front.index', compact('products','categories','pocetna','bestseller','banners'));
    }

    public function category($id){
        $products = Product::where('category1',$id)
            ->orWhere('category2',$id)
            ->where('available','Da')
            ->paginate(18);

        $category = Category::with('childrenCategories')->get();
        $categories = [];
        foreach ($category as $cat){

            $categories[]= $cat;
        }
        $cat = Category::where('name',$id)->first();

        $banners = Banner::all();

        return view('front.index', compact('products','categories','cat','banners'));

    }

    public function search(){
        $keyword =$_GET['keyword'];
        $products = Product::where('name','like', '%' . $keyword . '%')
            ->get();
        $category = Category::with('childrenCategories')->get();
        $categories = [];
        foreach ($category as $cat){

            $categories[]= $cat;
        }
        $banners = Banner::all();

        return view('front.index', compact('products','categories','banners'));

    }

    public function onaction(){
        $products = Product::where('on_action',true)
            ->where('available','Da')
            ->paginate(18);

        $category = Category::with('childrenCategories')->get();
        $categories = [];
        foreach ($category as $cat){

            $categories[]= $cat;
        }
        $banners = Banner::all();

        return view('front.index', compact('products','categories','banners'));

    }

    public function product($id){

        $products = Product::where('id',$id)
            ->first();

        $category = Category::with('childrenCategories')->get();
        $categories = [];
        foreach ($category as $cat){

            $categories[]= $cat;
        }
        $banners = Banner::all();

        return view('front.product', compact('products','categories','banners'));

    }
}
