<?php


/*
 * Admin Routes
 */
Route::prefix('admin')->group(function() {

    Route::middleware('auth:admin')->group(function() {
        // Dashboard
        Route::get('/', 'DashboardController@index')->name('admin');

        // Products
        Route::resource('/products','ProductController');

        // Orders
        Route::resource('/orders','OrderController');
        Route::get('/confirm/{id}','OrderController@confirm')->name('order.confirm');
        Route::get('/pending/{id}','OrderController@pending')->name('order.pending');

        // Users
        Route::resource('/users','UsersController');

        // Logout
        Route::get('/logout','AdminUserController@logout');

        Route::get('/parse','ParseXMLController@parse')->name('parse.view');
        Route::post('/update','ParseXMLController@parseXML')->name('parseXML');

        Route::post('/action','ProductController@actionCreate');
        Route::get('/search','ProductController@search')->name('admin/search');
        Route::get('/category/{id}','ProductController@category')->name('admin/category');
        Route::get('/actions','ProductController@actions');
        Route::delete('actions/destroy/{product_id}','ProductController@deleteAction')->name('admin.action');
        Route::post('/bestseller/{code}','BestsellerController@makeBestsellers')->name('makeBestseller');
        Route::get('/bestsellers','BestsellerController@getBestsellers')->name('getBestsellers');
        Route::delete('bestsellers/{code}','BestsellerController@removeFromBestsellers')->name('bestseller.destroy');
        Route::get('banners','BannersController@index');
        Route::post('addBanner','BannersController@addBanner');



    });

    // Admin Login
    Route::get('/login', 'AdminUserController@index');
    Route::post('/login', 'AdminUserController@store');
});

/*
 * Front Routes
 */

Route::get('/', 'Front\HomeController@index');
Route::get('/category/{id}','Front\HomeController@category')->name('category');
Route::get('/search','Front\HomeController@search')->name('search');
Route::get('/onaction','Front\HomeController@onaction')->name('onaction');


// User Registration
Route::get('/user/register','Front\RegistrationController@index');
Route::post('/user/register','Front\RegistrationController@store');

// User Login
Route::get('/user/login','Front\SessionsController@index');
Route::post('/user/login','Front\SessionsController@store');

// Logout
Route::get('/user/logout','Front\SessionsController@logout');

Route::get('/user/profile', 'Front\UserProfileController@index');
Route::get('/user/order/{id}','Front\UserProfileController@show');

// Cart
Route::get('/cart', 'Front\CartController@index');
Route::post('/cart','Front\CartController@store')->name('cart');
Route::patch('/cart/update/{product}','Front\CartController@update')->name('cart.update');
Route::delete('/cart/remove/{product}','Front\CartController@destroy')->name('cart.destroy');
Route::post('/cart/saveLater/{product}', 'Front\CartController@saveLater')->name('cart.saveLater');


// Save for later
Route::delete('/saveLater/destroy/{product}','Front\SaveLaterController@destroy')->name('saveLater.destroy');
Route::post('/cart/moveToCart/{product}','Front\SaveLaterController@moveToCart')->name('moveToCart');

// Checkout
Route::get('/checkout','Front\CheckoutController@index');
Route::post('/checkout','Front\CheckoutController@store')->name('checkout');

Route::get('empty', function() {
    Cart::instance('default')->destroy();
});
